import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import VuexPersistence from '@/plugins/vuex-persist';
import { RootState } from '@/store/types';

Vue.use(Vuex);

const store: StoreOptions<RootState> = {
  state: {
    appName: 'Vuejs + Vuex + TypeScript',
    appVersion: '1.0.0'
  },
  modules: {

  },
  plugins: [VuexPersistence.plugin]
};

export default new Vuex.Store<RootState>(store);
